var debug = require('debug');

var log = debug('open-weather-map-api-client');
var error = debug('open-weather-map-api-client-error');
var warn = debug('open-weather-map-api-client-error');

error.log = console.error.bind(console);
warn.log = console.warn.bind(console);

var request = require('request-promise');
var URI = require('URIjs');

var client = function(options) {

	options = merge({
		url: 'http://api.openweathermap.org/data/2.5'
		apiKey: ''
	}, options);

	return {
		current: {
			get: function(params) {

				var endPoint = URI(options.url).directory('weather').query(merge({APPID: options.apiKey}, params).toString();
				log('current:get URL ', endPoint);

				return request({uri: endPoint,	method: 'GET' });
			}
		}
	};
}

module.exports = client;
